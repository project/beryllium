Opinionated admin experience for Drupal.

Depends on bootstrap 3.19

Needs patch:
https://www.drupal.org/files/issues/2019-05-24/modal-height-3048592-4.patch
https://www.drupal.org/node/3048592
