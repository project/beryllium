<?php
/**
 * @file
 * Contains \Drupal\beryllium\Plugin\Preprocess\MenuLocalAction.
 */

namespace Drupal\beryllium\Plugin\Preprocess;

use Drupal\core\Plugin\PluginBase;
use Drupal\bootstrap\Plugin\Preprocess\PreprocessInterface;

/**
 * Pre-processes variables for the "menu_local_action" theme hook.
 *
 * @ingroup plugins_preprocess
 *
 * @BootstrapPreprocess("menu_local_action")
 */
class MenuLocalAction extends PluginBase implements PreprocessInterface {

  /**
   * {@inheritdoc}
   */
  public function preprocess(array &$variables, $hook, array $info) {
    $classes = isset($variables['link']['#options']['attributes']['class']) && is_array($variables['link']['#options']['attributes']['class']) ? $variables['link']['#options']['attributes']['class'] : [];
    $classes = array_filter($classes, function($e) { return !in_array($e, ['button', 'button-action']); });
    $variables['link']['#options']['attributes']['class'] = $classes + [
      'btn',
      'btn-primary',
      'btn-lg',
      'beryllium-btn-add',
    ];
  }
}
