<?php
/**
 * @file
 * Contains \Drupal\beryllium\Plugin\Preprocess\Html.
 */

namespace Drupal\beryllium\Plugin\Preprocess;

use Drupal\core\Plugin\PluginBase;
use Drupal\bootstrap\Plugin\Preprocess\PreprocessInterface;

/**
 * Pre-processes variables for the "html" theme hook.
 *
 * @ingroup plugins_preprocess
 *
 * @BootstrapPreprocess("html")
 */
class Html extends PluginBase implements PreprocessInterface {

  /**
   * {@inheritdoc}
   */
  public function preprocess(array &$variables, $hook, array $info) {
    $route = \Drupal::routeMatch()->getRouteName();

    $no_white_bg = [
      'node.add',
      'entity.node.edit_form',
      'node.add_page'
    ];
    if (in_array($route, $no_white_bg)) {
      $variables['attributes']['class'][] = 'no-white-bg';
    }

    $authoring = [
      'node.add',
      'entity.node.edit_form',
      'node.add_page'
    ];
    if (in_array($route, $authoring)) {
      $variables['attributes']['class'][] = 'authoring';
    }
  }

}
