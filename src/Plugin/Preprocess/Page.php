<?php
/**
 * @file
 * Contains \Drupal\beryllium\Plugin\Preprocess\Page.
 */

namespace Drupal\beryllium\Plugin\Preprocess;

use Drupal\block\Entity\Block;
use Drupal\Core\Render\Element;
use Drupal\core\Plugin\PluginBase;
use Drupal\bootstrap\Plugin\Preprocess\PreprocessInterface;

/**
 * Pre-processes variables for the "page" theme hook.
 *
 * @ingroup plugins_preprocess
 *
 * @BootstrapPreprocess("page")
 */
class Page extends PluginBase implements PreprocessInterface {

  /**
   * {@inheritdoc}
   */
  public function preprocess(array &$variables, $hook, array $info) {
    $route = \Drupal::routeMatch()->getRouteName();

    $variables['nav_tabs_right'] = FALSE;
    $variables['page_classes'] = '';

    $block = Block::load('beryllium_messages');
    $variables['messages'] = \Drupal::entityTypeManager()
      ->getViewBuilder('block')
      ->view($block);

    if (isset($variables['page']['header']['beryllium_messages'])) {
      unset($variables['page']['header']['beryllium_messages']);
    }

    if (isset($variables['page']['header']['beryllium_local_actions'])) {
      unset($variables['page']['header']['beryllium_local_actions']);
    }

    $variables['local_actions'] = beryllium_get_local_actions_block();

    // Separate primary and secondary nav tabs
    $variables['nav_tabs'] = $this->getLocalTasks(0);
    $variables['secondary_nav_tabs'] = $this->getLocalTasks(1);
    if (isset($variables['page']['header']['beryllium_local_tasks'])) {
      unset($variables['page']['header']['beryllium_local_tasks']);
    }

    if (in_array($route, ['block.admin_display', 'block.admin_display_theme'])) {
      $demo_theme = \Drupal::routeMatch()->getParameter('theme') ?: \Drupal::config('system.theme')->get('default');
      $themes = \Drupal::service('theme_handler')->listInfo();
      // $variables['nav_tabs_right'] = \Drupal::l(t('<span class="beryllium-icon-question-mark dark-help-icon"></span>Demonstrate block regions (@theme)', ['@theme' => $themes[$demo_theme]->info['name']]), new Url('block.admin_demo', ['theme' => $demo_theme]));
    }
  }

  private function getLocalTasks(int $depth) {
    $manager = \Drupal::service('plugin.manager.menu.local_task');
    $links = $manager->getLocalTasks(\Drupal::routeMatch()->getRouteName(), $depth);
    return count(Element::getVisibleChildren($links['tabs'])) > 1 ? $links['tabs'] : '';
  }
  
}
