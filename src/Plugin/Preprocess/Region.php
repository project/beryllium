<?php
/**
 * @file
 * Contains \Drupal\beryllium\Plugin\Preprocess\Region.
 */

namespace Drupal\beryllium\Plugin\Preprocess;

use Drupal\core\Plugin\PluginBase;
use Drupal\bootstrap\Plugin\Preprocess\PreprocessInterface;

/**
 * Pre-processes variables for the "region" theme hook.
 *
 * @ingroup plugins_preprocess
 *
 * @BootstrapPreprocess("region")
 */
class Region extends PluginBase implements PreprocessInterface {

  /**
   * {@inheritdoc}
   */
  public function preprocess(array &$variables, $hook, array $info) {
    if ($variables['region'] == 'help') {
      if (isset($variables['attributes']['class']) && in_array('alert-info', $variables['attributes']['class'])) {
        $route = \Drupal::routeMatch()->getRouteName();
        if (in_array($route, ['block.admin_display', 'block.admin_display_theme'])) {
          $variables['content']['content']['#markup'] = '<p>' . t('Block placement is specific to each theme on your site. Changes will not be saved until you click <em>Save blocks</em> at the bottom of the page.') . '</p>';
        }

        // Remove icon from help region when it has the "alert-info" class
        unset($variables['content']['icon']);
      }
    }
  }

}
