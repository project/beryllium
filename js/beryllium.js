(function ($, Drupal, document, CKEDITOR) {
  Drupal.behaviors.brlmMatchHeight = {
    attach: function (context, settings) {
      if ($('.admin-add-content-list').length > 0) {
        $('.content-type-tile').matchHeight({ byRow: false });
      }

      if ($('.js-media-library-views-form').length > 0) {
        $('.js-media-library-item:not(.media-library-item--table)').matchHeight({ byRow: true});
      }
    }
  };

  Drupal.behaviors.brlmStickyActions = {
    attach: function(context, settings) {
      $('.sidebar-actions').once('brlmStickyActions').each(function() {
        var $this = $(this), $window = $(window), offsetTop = $this.offset().top, height = $this.outerHeight(), width = Math.ceil($this.parent().width()) + 2;
        $this.css({'width': width + 'px'});

        if (Drupal.displace && Drupal.displace.offsets) {
          $this.css({'top': Drupal.displace.offsets.top + 'px'});
        }
        $window
          .on('scroll', function(e) {
            if ($window.scrollTop() > offsetTop) {
              $this.css({'position': 'fixed'}).parent().css({'padding-top': height + 'px'});
            }
            else {
              $this.css({'position': ''}).parent().css({'padding-top': ''});;
            }
          })
          .on('resize', function(e) {
            $this.css({'width': width + 'px'});
          });
        $(document).on('drupalViewportOffsetChange', function(e, offsets) {
          $this.css({'top': offsets.top + 'px', 'width': width + 'px'});
        })
      });
    }
  }

  Drupal.behaviors.brlmHideMachineName = {
    attach: function(context, settings) {
      if($('#edit-label-machine-name-suffix', context).length > 0) {
        var target = document.getElementById('edit-label-machine-name-suffix');
        var observer = new MutationObserver(function(mutations) {
          mutations.forEach(function(mutationRecord) {
            var machineNameDOM = $(mutationRecord.target);
            machineNameDOM.parent().css({'opacity': 0});
            if(machineNameDOM.attr('style') != 'display: none;') {
              machineNameDOM.parent().css({'opacity': 1});
            }
          });
        });
        observer.observe(target, { attributes: true, attributeFilter: ['style'] });
      }
    }
  }

  Drupal.behaviors.brlmHideThrobber = {
    attach: function(context, settings) {
      if($('.ajax-progress-throbber', context).length > 0) {
        var observer = new MutationObserver(function(mutations) {
          mutations.forEach(function(mutationRecord) {
            var throbberDOM = $(mutationRecord.target);
            if(throbberDOM.is('.glyphicon-spin')) {
              throbberDOM.parent().show();
            }
          });
        });
        $('.ajax-progress-throbber', context).each(function(){
          $(this).parent().hide();
          observer.observe(this, { attributes: true, attributeFilter: ['class'] });
        });
      }
    }
  }

  Drupal.behaviors.brlmViewsMoveActionList = {
    attach: function(context, settings) {
      var actionList = $('#views-display-menu-tabs li.add ul.action-list');
      if(actionList.length > 0) {
        $('#views-display-top').once('move-add-display').find('button').each(function(){
          if($(this).hasClass('add-display')) {
            $(this).detach();
            $(this).appendTo(actionList);
          }
        });
      }
    }
  }

  Drupal.behaviors.brlmViewsFixOptionsDropButton = {
    attach: function(context, settings) {
      $('#edit-display-settings-top .custom-dropdown ul.dropbutton', context).once('fix-options-dropdown').removeClass('dropbutton').addClass('dropdown-menu');
    }
  }

  Drupal.behaviors.brlmViewsSetTabDropButtonGroups = {
    attach: function(context, settings) {
      $('#edit-display-settings-main .dropdown', context).once('set-tab-dropbutton-groups').each(function() {
        if($(this).find('button').length < 2) {
          $(this).removeClass('btn-group');
        }
        else {
          $(this).addClass('btn-group');
        }
      });
    }
  }
  /**
   * Registers behaviours related to modal open and windows resize for fluid modal.
   */
  Drupal.behaviors.berylliumFluidModal = {
    attach: function (context) {

      // Recalculate dialog size on window resize.
      $(window).resize(function (context) {
        Drupal.brlm.fluidDialog();
      });

      // Catch dialog if opened within a viewport smaller than the dialog width
      // and recalculate size of all open dialogs.
      $(document).on('dialogopen', '.modal', function (event, ui) {
        Drupal.brlm.fluidDialog(event.target);
      });

    }
  };

  Drupal.brlm =  Drupal.brlm || {};
    /**
   * Recalculates size of the modal.
   */
  Drupal.brlm.fluidDialog = function (elem) {
    var $elem = $(elem);
    if (!$(elem).is(':visible')) {
      return;
    }

    var modal = $elem.data('bs.modal'),
      options = modal.options.dialogOptions,
      $target = $elem.find('.ui-dialog');
    // If fluid option == true.
    if (options.fluid) {
      var wWidth = $(window).width();
      // Check window width against dialog width.
      if (options.maxWidth && (wWidth > parseInt(options.maxWidth) + 50)) {
        $target.css('width', options.maxWidth);
      }
      else {
        // If no maxWidth is defined, make it responsive.
        $target.css('width', '92%');
      }

      var vHeight = $(window).height();
      // Check window width against dialog width.
      if (options.maxHeight && vHeight > (parseInt(options.maxHeight) + 50)) {
        $target.css('height', options.maxHeight);
      }
      else {
        // If no maxHeight is defined, make it responsive.
        $target.css('height', vHeight - 100);

        // Because there is no iframe height 100% in HTML 5, we have to set
        // the height of the iframe as well.
        var contentHeight = $this.find('.ui-dialog-content').height() - 20;
        $this.find('iframe').css('height', contentHeight);
      }

      // Reposition dialog.
      dialog.option('position', dialog.options.position);
    }
  };

  Drupal.behaviors.brlmSplitCheckboxesAddCheckAll = {
  attach: function(context, settings) {
    $('.node-form .form-checkboxes', context).once('split-checkboxes-add-check-all').each(function() {
      var checkboxCount = $(this).find('input[type="checkbox"]').length;
      // add check all functionality if list has more than 9 items
      if (checkboxCount >= 10) {
        $(this).parent().prepend('<div class="check-all-wrapper"><label><input type="checkbox" class="check-all" /> <span class="select-all-label">Select All</span></label></div>');
        $('.check-all').change(function(){
          var checkedStatus = $(this).prop('checked');
          $(this).parents('.check-all-wrapper').siblings('.form-checkboxes').find('input[type="checkbox"]').each(function(){
            $(this).prop('checked', checkedStatus);
          });
        });
      }
      // deselect check all if anything in the list is deselected
      $('input[type="checkbox"]').change(function(){
        if ($(this).prop('checked') == false && $(this).parents('.form-checkboxes').siblings('.check-all-wrapper').find('.check-all').prop('checked') == true) {
          $(this).parents('.form-checkboxes').siblings('.check-all-wrapper').find('.check-all').prop('checked', false);
        }
      });
      // split checkboxes into columns based on number
      if(checkboxCount >= 20) {
        $(this).css({'column-count': 4});
      }
      else if (checkboxCount >= 15) {
        $(this).css({'column-count': 3});
      }
      else if (checkboxCount >= 10) {
        $(this).css({'column-count': 2});
      }
    });
  }
}

// (fairly) gracefully handle z-index stacking for multiple modals
$(document).once('brlmMultipleModals').on('show.bs.modal', '.modal', function() {
  var that = this;
  var maxzIndex = Math.max.apply(null, $('.modal').toArray().map(function(b) {
    return b === that ? 0 : $(b).css('zIndex')
  })) || 1050;

  // If CKEditor is maximized, add the ckeditor base z index, too.
  if (window.CKEDITOR && window.CKEDITOR.hasOwnProperty('config')) {
    var baseFloatZIndex = window.CKEDITOR.config.baseFloatZIndex || 100000;
    maxzIndex+=baseFloatZIndex;
  }

  this.style.setProperty('z-index', maxzIndex+2, 'important');
  (function(modal) {
    window.setTimeout(function() {
      if (modal.$backdrop && modal.$backdrop.length>0) {
        modal.$backdrop.get(0).style.setProperty('z-index', maxzIndex+1, 'important');
      }
    }, 10)
  })($(this).data('bs.modal'))

}).on('hidden.bs.modal', function() {
  // If we close a modal, it removes the modal-open class from body. Add it back in
  // if there's still an active modal.
  $('.modal:visible').first().each(function() {
    var modal = $(this).data('bs.modal');
    modal.checkScrollbar();
    modal.setScrollbar();
    $('body').addClass('modal-open').css('overflow', 'hidden');
  });
});

})(jQuery, Drupal, document);

/*
* jquery-match-height 0.7.0 by @liabru
* http://brm.io/jquery-match-height/
* License MIT
*/
!function(t){"use strict";"function"==typeof define&&define.amd?define(["jquery"],t):"undefined"!=typeof module&&module.exports?module.exports=t(require("jquery")):t(jQuery)}(function(t){var e=-1,o=-1,i=function(t){return parseFloat(t)||0},a=function(e){var o=1,a=t(e),n=null,r=[];return a.each(function(){var e=t(this),a=e.offset().top-i(e.css("margin-top")),s=r.length>0?r[r.length-1]:null;null===s?r.push(e):Math.floor(Math.abs(n-a))<=o?r[r.length-1]=s.add(e):r.push(e),n=a}),r},n=function(e){var o={
byRow:!0,property:"height",target:null,remove:!1};return"object"==typeof e?t.extend(o,e):("boolean"==typeof e?o.byRow=e:"remove"===e&&(o.remove=!0),o)},r=t.fn.matchHeight=function(e){var o=n(e);if(o.remove){var i=this;return this.css(o.property,""),t.each(r._groups,function(t,e){e.elements=e.elements.not(i)}),this}return this.length<=1&&!o.target?this:(r._groups.push({elements:this,options:o}),r._apply(this,o),this)};r.version="0.7.0",r._groups=[],r._throttle=80,r._maintainScroll=!1,r._beforeUpdate=null,
r._afterUpdate=null,r._rows=a,r._parse=i,r._parseOptions=n,r._apply=function(e,o){var s=n(o),h=t(e),l=[h],c=t(window).scrollTop(),p=t("html").outerHeight(!0),d=h.parents().filter(":hidden");return d.each(function(){var e=t(this);e.data("style-cache",e.attr("style"))}),d.css("display","block"),s.byRow&&!s.target&&(h.each(function(){var e=t(this),o=e.css("display");"inline-block"!==o&&"flex"!==o&&"inline-flex"!==o&&(o="block"),e.data("style-cache",e.attr("style")),e.css({display:o,"padding-top":"0",
"padding-bottom":"0","margin-top":"0","margin-bottom":"0","border-top-width":"0","border-bottom-width":"0",height:"100px",overflow:"hidden"})}),l=a(h),h.each(function(){var e=t(this);e.attr("style",e.data("style-cache")||"")})),t.each(l,function(e,o){var a=t(o),n=0;if(s.target)n=s.target.outerHeight(!1);else{if(s.byRow&&a.length<=1)return void a.css(s.property,"");a.each(function(){var e=t(this),o=e.attr("style"),i=e.css("display");"inline-block"!==i&&"flex"!==i&&"inline-flex"!==i&&(i="block");var a={
display:i};a[s.property]="",e.css(a),e.outerHeight(!1)>n&&(n=e.outerHeight(!1)),o?e.attr("style",o):e.css("display","")})}a.each(function(){var e=t(this),o=0;s.target&&e.is(s.target)||("border-box"!==e.css("box-sizing")&&(o+=i(e.css("border-top-width"))+i(e.css("border-bottom-width")),o+=i(e.css("padding-top"))+i(e.css("padding-bottom"))),e.css(s.property,n-o+"px"))})}),d.each(function(){var e=t(this);e.attr("style",e.data("style-cache")||null)}),r._maintainScroll&&t(window).scrollTop(c/p*t("html").outerHeight(!0)),
this},r._applyDataApi=function(){var e={};t("[data-match-height], [data-mh]").each(function(){var o=t(this),i=o.attr("data-mh")||o.attr("data-match-height");i in e?e[i]=e[i].add(o):e[i]=o}),t.each(e,function(){this.matchHeight(!0)})};var s=function(e){r._beforeUpdate&&r._beforeUpdate(e,r._groups),t.each(r._groups,function(){r._apply(this.elements,this.options)}),r._afterUpdate&&r._afterUpdate(e,r._groups)};r._update=function(i,a){if(a&&"resize"===a.type){var n=t(window).width();if(n===e)return;e=n;
}i?-1===o&&(o=setTimeout(function(){s(a),o=-1},r._throttle)):s(a)},t(r._applyDataApi),t(window).bind("load",function(t){r._update(!1,t)}),t(window).bind("resize orientationchange",function(t){r._update(!0,t)})});
