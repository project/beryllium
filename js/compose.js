(function ($, Drupal) {

Drupal.theme.composeSortHandle = function() {
  return '<a href="#" class="tabledrag-handle"><span class="icon glyphicon glyphicon-move" aria-hidden="true"></span></a>';
}

})(jQuery, Drupal);
