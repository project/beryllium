(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.brlmSelect2 = {
    attach: function (context, settings) {
      $('select').once('select2').each(function () {
        var $this = $(this);
        if ($this.find('option').length > 10 || $this.attr('multiple')) {
          $this.select2({
            theme: "bootstrap",
            dropdownAutoWidth: true,
            width: 'auto'
          });
        }
      });
    }
  }

})(jQuery, Drupal, drupalSettings);
